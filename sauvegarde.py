import os
from git import Repo, GitCommandError

# Chemin vers votre dépôt Git local
repo_path = "C:\\Users\\Utilisateur\\Desktop\\Projet_MSPR6.1\\mspr6.1"
repo = Repo(repo_path)
assert not repo.bare

# Chemin vers le dossier contenant les fichiers JSON à sauvegarder
backup_folder ="C:\\Users\\Utilisateur\\Desktop\\Projet_MSPR6.1\\mspr6.1\\dossier_de_sauvegarde"# Si c'est le même que le repo_path

# Liste tous les fichiers .json dans le dossier de sauvegarde
files_to_backup = [f for f in os.listdir(backup_folder) if f.endswith('.json')]

# Ajoute chaque fichier .json au dépôt et crée un commit
changes_made = False
for file_name in files_to_backup:
    file_path = os.path.join(backup_folder, file_name)
    if os.path.isfile(file_path):
        # Ajoute le fichier au staging area
        repo.index.add([file_path])
        changes_made = True

# Effectue un commit si des changements ont été effectués
if changes_made:
    repo.index.commit('Sauvegarde automatique des fichiers JSON')

# Tente de résoudre les problèmes de push
try:
    origin = repo.remote(name='origin')
    # Récupère la dernière version du dépôt distant avant de pousser
    origin.pull(rebase=True)
    # Tente de pousser les changements
    origin.push()
    print("Les fichiers JSON ont été sauvegardés dans le dépôt distant.")
except GitCommandError as e:
    print(f"Une erreur est survenue lors du push : {e}")
