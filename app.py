from flask import Flask, render_template, request, flash, redirect, url_for, jsonify
import os
import json
from flask import send_from_directory
import subprocess
from git import Repo, GitCommandError
import nmap


app = Flask(__name__)
app.config.from_pyfile('config.py')  # Load configurations from a separate file

# Endpoint for the dashboard
@app.route('/')
def index():
    return render_template('dashboard.html')

# Endpoint to initiate network scan
@app.route('/scan', methods=['POST'])
def scan_network():
    ip_address = request.form.get('ip_address')
    if not ip_address:
        flash('Aucune adresse IP fournie.', 'warning')
        return redirect(url_for('index'))
    
    nm = nmap.PortScanner()
    try:
        nm.scan(hosts=ip_address, arguments='-sT')
    except nmap.PortScannerError as e:
        flash(f'Erreur lors du scan: {e}', 'danger')
        return redirect(url_for('index'))

    active_hosts = nm.all_hosts()
    scan_results = {
        host: {
            "state": nm[host].state(),
            "ports": nm[host]['tcp'] if 'tcp' in nm[host].all_protocols() else []
        } for host in active_hosts
    }

    # Directory where scan results will be saved
    dossier_sauvegarde = app.config['DOSSIER_DE_SAUVEGARDE']  # Make sure this is defined in your config.py
    if not os.path.exists(dossier_sauvegarde):
        os.makedirs(dossier_sauvegarde)

    file_name = f"scan_results_{ip_address.replace('/', '_').replace('.', '_')}.json"  # Create a file name that is safe for file names
    save_results(scan_results, dossier_sauvegarde, file_name)

    flash('Le scan du réseau a été complété avec succès. Résultats sauvegardés.', 'success')
    return redirect(url_for('index'))

def save_results(scan_results, folder_path, file_name):
    full_path = os.path.join(folder_path, file_name)
    with open(full_path, 'w') as file:
        json.dump(scan_results, file, indent=4)

@app.route('/backup', methods=['POST'])
def sauvegarde():
    chemin_repo = app.config['CHEMIN_REPO']
    dossier_sauvegarde = os.path.join(chemin_repo, 'dossier_de_sauvegarde')

    repo = Repo(chemin_repo)
    assert not repo.bare

    fichiers_a_sauvegarder = [f for f in os.listdir(dossier_sauvegarde) if f.endswith('.json')]

    modifications_apportées = False
    for nom_fichier in fichiers_a_sauvegarder:
        chemin_fichier_rel = os.path.join('dossier_de_sauvegarde', nom_fichier)
        chemin_fichier_abs = os.path.join(dossier_sauvegarde, nom_fichier)
        if os.path.isfile(chemin_fichier_abs):
            repo.index.add([chemin_fichier_rel])
            modifications_apportées = True

    if modifications_apportées:
        try:
            repo.index.commit('Sauvegarde automatique des fichiers JSON')
            origin = repo.remote(name='origin')
            origin.pull(rebase=True)
            origin.push()
            flash("Les fichiers JSON ont été sauvegardés dans le dépôt distant.", 'success')
        except GitCommandError as e:
            flash(f"Erreur Git lors de la sauvegarde: {e}", 'danger')
    else:
        flash("Aucun fichier JSON modifié à sauvegarder.", 'info')

    return redirect(url_for('index'))

@app.route('/ping', methods=['GET', 'POST'])
def ping():
    ping_result = None
    if request.method == 'POST':
        host = request.form.get('target')
        if not host:
            flash('Aucune adresse IP fournie.', 'warning')
        else:
            command = ['ping', '-c', '1', host] if os.name != 'nt' else ['ping', '-n', '1', host]
            try:
                response = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, timeout=5)
                ping_result = "En ligne" if response.returncode == 0 else "Hors ligne"
            except subprocess.TimeoutExpired:
                ping_result = "Temps d'attente dépassé"
        return render_template('dashboard.html', ping_result=ping_result, host=host)
    return render_template('dashboard.html', ping_result=ping_result)

@app.route('/all_scans')
def all_scans():
    dossier_sauvegarde = "C:/Users/Utilisateur/Desktop/Projet_MSPR6.1/mspr6.1/dossier_de_sauvegarde"
    try:
        # Listez tous les fichiers dans le dossier
        files = [f for f in os.listdir(dossier_sauvegarde) if f.endswith('.json')]
        return jsonify(files)
    except Exception as e:
        # En cas d'erreur, renvoyez un message d'erreur
        return jsonify({"error": str(e)}), 500
    
@app.route('/scans/<filename>')
def get_scan(filename):
    dossier_sauvegarde = "C:/Users/Utilisateur/Desktop/Projet_MSPR6.1/mspr6.1/dossier_de_sauvegarde"
    filepath = os.path.join(dossier_sauvegarde, filename)
    print(f"Recherche du fichier : {filepath}")  # Instruction de débogage
    try:
        if os.path.isfile(filepath):
            print(f"Envoi du fichier : {filepath}")  # Instruction de débogage
            return send_from_directory(dossier_sauvegarde, filename)
        else:
            print(f"Fichier non trouvé : {filepath}")  # Instruction de débogage
            return jsonify({"error": "Fichier non trouvé"}), 404
    except Exception as e:
        print(f"Erreur lors de la tentative d'envoi du fichier : {e}")  # Instruction de débogage
        return jsonify({"error": str(e)}), 500
    
@app.route('/delete_scan/<filename>', methods=['POST'])
def delete_scan(filename):
    dossier_sauvegarde = "C:/Users/Utilisateur/Desktop/Projet_MSPR6.1/mspr6.1/dossier_de_sauvegarde"
    filepath = os.path.join(dossier_sauvegarde, filename)
    try:
        if os.path.isfile(filepath):
            os.remove(filepath)
            return jsonify({"success": f"Le fichier {filename} a été supprimé avec succès."})
        else:
            return jsonify({"error": "Fichier non trouvé"}), 404
    except Exception as e:
        return jsonify({"error": str(e)}), 500


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)