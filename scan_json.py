import json
import os
from git import Repo
import nmap

def scan_network(ip_address):
    nm = nmap.PortScanner()
    nm.scan(hosts=ip_address, arguments='-sT')
    active_hosts = nm.all_hosts()

    scan_results = {}
    for host in active_hosts:
        host_info = nm[host]
        scan_results[host] = {
            "state": host_info.state(),
            "ports": host_info['tcp'] if 'tcp' in host_info else {}
        }
    return scan_results

def save_results(scan_results, folder_path, file_name):
    # Chemin complet du fichier JSON
    full_path = os.path.join(folder_path, file_name)
    with open(full_path, 'w') as file:
        json.dump(scan_results, file, indent=4)

def git_operations(repo_path, file_path, commit_message):
    repo = Repo(repo_path)
    repo.git.add(file_path)
    repo.index.commit(commit_message)
    origin = repo.remote(name='origin')
    origin.push()

def main():
    ip_address = input("IP à scanner (exemple: 192.168.1.1/24): ")
    json_file_name = input("Entrez le nom du fichier JSON pour les résultats: ").strip()
    if not json_file_name:
        json_file_name = "scan_results"  # Nom par défaut si l'entrée est vide
    json_file_path = f"{json_file_name}.json"

    repo_path = "C:/Users/Utilisateur/Desktop/Projet_MSPR6.1/mspr6.1"  # Chemin vers votre dépôt Git
    dossier_sauvegarde = "C:/Users/Utilisateur/Desktop/Projet_MSPR6.1/mspr6.1/dossier_de_sauvegarde"  # Dossier pour sauvegarder les résultats

    scan_results = scan_network(ip_address)
    save_results(scan_results, dossier_sauvegarde, json_file_path)

    git_username = "Jean Martial"
    git_email = "j.lendira1@ecoles-epsi.net"

    # Configuration de l'utilisateur Git
    repo = Repo(repo_path)
    repo.config_writer().set_value("user", "name", git_username).release()
    repo.config_writer().set_value("user", "email", git_email).release()

    commit_message = f"Mise à jour des résultats du scan réseau : {json_file_name}"
    # Notez que l'ajout se fait avec le chemin relatif par rapport à la racine du dépôt
    git_operations(repo_path, os.path.join('dossier_resultats', json_file_path), commit_message)

if __name__ == "__main__":
    main()


