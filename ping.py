import subprocess
import tkinter as tk
from tkinter import ttk

# Fonction pour pinger l'adresse saisie
def ping():
    host = address_entry.get()
    result_text.delete(1.0, tk.END)  # Efface le contenu précédent de la zone de texte
    if host:
        # Détermine la commande ping en fonction du système d'exploitation
        command = ['ping', '-c', '4', host] if tk.sys.platform != "win32" else ['ping', '-n', '4', host]
        
        # Exécute la commande ping
        try:
            response = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, timeout=10)
            # Affiche le résultat dans la zone de texte
            result_text.insert(tk.END, response.stdout + "\n")
            if response.returncode == 0:
                result_text.insert(tk.END, f"{host} est accessible.\n")
            else:
                result_text.insert(tk.END, f"{host} n'est pas accessible.\n")
        except subprocess.TimeoutExpired:
            result_text.insert(tk.END, "La commande ping a dépassé le délai d'attente.\n")

# Crée l'interface graphique
root = tk.Tk()
root.title("Ping Serveur")

# Définit la géométrie de la fenêtre
root.geometry("600x400")

# Crée un cadre pour l'entrée de l'adresse
frame = ttk.Frame(root)
frame.pack(pady=20)

# Crée une entrée de texte pour l'adresse
address_label = ttk.Label(frame, text="Adresse IP:")
address_label.pack(side=tk.LEFT)
address_entry = ttk.Entry(frame, width=40)
address_entry.pack(side=tk.LEFT)

# Crée un bouton pour exécuter le ping
ping_button = ttk.Button(frame, text="Ping", command=ping)
ping_button.pack(side=tk.LEFT, padx=10)

# Crée une zone de texte pour afficher les résultats
result_text = tk.Text(root, height=15)
result_text.pack(pady=10, fill=tk.BOTH, expand=True)

root.mainloop()
