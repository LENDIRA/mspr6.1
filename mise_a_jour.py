import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import git
import os

# Chemin du dépôt Git distant
repo_url = "https://gitlab.com/LENDIRA/mspr6.1"

# Chemin du dossier de l'application où le dépôt Git sera cloné
repo_path = "C:\\Users\\Utilisateur\\Desktop\\Projet_MSPR6.1\\mspr6.1"

# Fonction pour sauvegarder le dépôt GitLab distant au local
def save_gitlab_to_local():
    try:
        # Vérifie si le dossier du dépôt Git existe déjà
        if os.path.exists(repo_path):
            # Le dossier existe, effectuer un 'git pull' pour mettre à jour
            repo = git.Repo(repo_path)
            repo.remotes.origin.pull()
            messagebox.showinfo("Succès", "Le dépôt a été mis à jour avec succès.")
        else:
            # Si le dossier n'existe pas, cloner le dépôt
            git.Repo.clone_from(repo_url, repo_path)
            messagebox.showinfo("Succès", "Le dépôt a été cloné avec succès.")
    except Exception as e:
        messagebox.showerror("Erreur", f"Une erreur est survenue lors de la sauvegarde : {e}")

# Crée la fenêtre principale de l'application
root = tk.Tk()
root.title("Sauvegarde GitLab")
root.geometry("300x100")

# Ajoute un bouton pour déclencher la sauvegarde du dépôt GitLab au local
save_btn = ttk.Button(root, text="Sauvegarder le dépôt", command=save_gitlab_to_local)
save_btn.pack(pady=20)

root.mainloop()  # Lance la boucle principale de l'application pour afficher la fenêtre